import "./App.scss"
import {GlobalContext} from "./context";
import {useState} from "react";
import Layout from "./Layout";

const accountName = "Ivan_Ivanov"
const accountPassword = "password123"

export default function App() {
    const [isLogin, setIsLogin] = useState<boolean>(false)

    const login = (name: string, password: string) => {
        if (!(
            /^[a-z]+([-_]?[a-z0-9]+){0,2}$/i.test(name)
            && /^(?=.*\d)\w{3,20}$/m.test(password)
        )) {
            logout()
            return false
        }

        if (name.toLowerCase() === accountName.toLowerCase() && password === accountPassword) {
            localStorage.setItem("name", name)
            localStorage.setItem("password", password)
            setIsLogin(true)
            return true
        } else {
            logout()
            return false
        }
    }

    const logout = () => {
        localStorage.removeItem("name")
        localStorage.removeItem("password")
        setIsLogin(false)
    }

    const checkLogin = () => {
        let name = localStorage.getItem("name")
        let password = localStorage.getItem("password")

        if(name && password)
            return login(name, password)
    }

    return (
        <GlobalContext.Provider value={{checkLogin, login, logout, isLogin, accountName}}>
            <Layout/>
        </GlobalContext.Provider>
    )
}