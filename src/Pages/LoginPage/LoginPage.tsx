import PreloaderImage from "../../Components/UI/PreloaderImage/PreloaderImage";
import {useContext, useState} from "react";
import {GlobalContext} from "../../context";
import {useNavigate} from "react-router-dom"
import "./LoginPage.scss"

export default function LoginPage() {
    const [name, setName] = useState<string>()
    const [password, setPassword] = useState<string>()
    const [isError, setIsError] = useState<boolean>(false)
    const {login} = useContext<any>(GlobalContext)
    const navigate = useNavigate()

    const formSubmit = () => {
        if ((
            name && password
            && login(name, password)
        )) {
            navigate("/")
        } else {
            setIsError(true)
        }
    }

    return (
        <section className="login-page">
            <PreloaderImage src={require("./images/image.webp")} alt=""
                            className="login-page__image"/>
            <div className="login-page__container">
                <form className="login-page__form"
                      onSubmit={(event: any) => {
                          event.preventDefault()
                          formSubmit()
                      }}>
                    <h1 className="login-page__header">LOG IN</h1>

                    <div className="login-page__form-inputs">
                        <input onChange={(event: any) => setName(event.target.value)} name="name"
                               aria-label="name"
                               placeholder="Enter your name" type="text"
                               className="input login-page__input"/>
                        <input onChange={(event: any) => setPassword(event.target.value)} name="password"
                               aria-label="password"
                               placeholder="Enter password" type="password"
                               className="input login-page__input"/>
                    </div>
                    {
                        isError
                            ?
                            <div role="alert" className="login-page__alert">
                                Wrong name or password. Please, try again
                            </div>
                            :
                            null
                    }
                    <button className="button login-page__submit">Continue</button>
                </form>
            </div>
        </section>
    )
}