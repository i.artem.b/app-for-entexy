import {useParams, Navigate} from "react-router-dom"
import "./ImageInfoPage.scss"
import {useEffect, useState} from "react";
import Preloader from "../../Components/UI/Preloader/Preloader";

enum ImageInfoPageLoadingState {
    loading,
    loaded,
    error
}

export default function ImageInfoPage() {
    const [loadingState, setLoadingState] = useState<ImageInfoPageLoadingState>(ImageInfoPageLoadingState.loading)
    const [imageInfo, setImageInfo] = useState<any>()
    const {id} = useParams()

    useEffect(() => {
        window.scrollTo(0,0)
        fetch(`https://picsum.photos/id/${id}/info`)
            .then(data => data.json())
            .then(data => {
                setImageInfo(data)
                setLoadingState(ImageInfoPageLoadingState.loaded)
            })
            .catch(() => setLoadingState(ImageInfoPageLoadingState.error))
    }, [id])

    const Layout = (
        <section className="image-info-page">
            <div className="image-info-page__container container">
                <img src={imageInfo?.download_url} style={{aspectRatio: `${imageInfo?.width} / ${imageInfo?.height}`}} alt={imageInfo?.author}
                     className="image-info-page__image"/>
                <div className="image-info-page__info">
                    <h2 className="image-info-page__info-header">Details</h2>
                    <table className="image-info-page__table">
                        <tbody>
                        <tr className="image-info-page__table-item">
                            <th scope="row" className="image-info-page__table-item-label">
                                Resolution
                            </th>
                            <th className="image-info-page__table-item-value">
                                {imageInfo?.width} x {imageInfo?.height}
                            </th>
                        </tr>
                        <tr className="image-info-page__table-item">
                            <th scope="row" className="image-info-page__table-item-label">
                                Author
                            </th>
                            <th className="image-info-page__table-item-value">
                                {imageInfo?.author}
                            </th>
                        </tr>
                        <tr className="image-info-page__table-item">
                            <th scope="row" className="image-info-page__table-item-label">
                                ID
                            </th>
                            <th className="image-info-page__table-item-value">
                                {imageInfo?.id}
                            </th>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    )
    const loadingLayout = <Preloader/>
    const errorLayout = <Navigate to="/404"/>

    let show = loadingLayout

    switch (loadingState) {
        case ImageInfoPageLoadingState.loading:
            show = loadingLayout
            break
        case ImageInfoPageLoadingState.loaded:
            show = Layout
            break
        case ImageInfoPageLoadingState.error:
            show = errorLayout
    }

    return show
}