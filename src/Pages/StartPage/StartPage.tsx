import StartBlock from "../../Components/Blocks/StartBlock/StartBlock";
import ImageListBlock from "../../Components/Blocks/ImageListBlock/ImageListBlock";

export default function StartPage() {
    return (
        <>
            <StartBlock scrollTarget="images"/>
            <ImageListBlock id="images"/>
        </>
    )
}