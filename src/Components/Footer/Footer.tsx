import "./Footer.scss"

export default function Footer() {

    return (
        <footer className="footer">
            <div className="footer__container container">
                <div className="footer__columns">
                    <section className="footer__column">
                        <h2 className="footer__column-header">Lorem</h2>
                        <p className="footer__column-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                            do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Faucibus nisl tincidunt eget nullam non nisi est sit
                            amet.</p>
                    </section>
                    <section className="footer__column">
                        <h2 className="footer__column-header">Lorem Ipsum</h2>
                        <p className="footer__column-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                            do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Faucibus nisl tincidunt eget nullam non nisi est sit amet.
                            Vulputate odio ut enim blandit volutpat maecenas volutpat. Massa tincidunt dui ut ornare
                            lectus sit. Quam elementum pulvinar etiam non.</p>
                    </section>
                    <section className="footer__column">
                        <h2 className="footer__column-header">Contacts</h2>
                        <ul className="footer__contacts-list">
                            <li className="footer__contacts-item">
                                <span className="footer__contact-header">London</span><br/>
                                <span
                                    className="footer__contact-address">26985 Brighton Lane, Lake Forest, CA 92630</span><br/>
                                <a className="footer__contact-phone" href="tel:+1 (949) 354-2574">+1 (949) 354-2574</a>
                            </li>
                            <li className="footer__contacts-item">
                                <span className="footer__contact-header">Paris</span><br/>
                                <span className="footer__contact-address">9 Doe Crossing Court</span><br/>
                                <a className="footer__contact-phone" href="tel:+1 (949) 354-2574">+11 281-762-2687</a>
                            </li>
                        </ul>
                    </section>
                </div>
                <ul className="footer__payments-list">
                    <li className="footer__payments-list-item">
                        <img className="footer__payment" src={require("./images/visa.png")} alt="Visa"/>
                    </li>
                    <li className="footer__payments-list-item">
                        <img className="footer__payment" src={require("./images/mastercard.png")} alt="Mastercard"/>
                    </li>
                    <li className="footer__payments-list-item">
                        <img className="footer__payment" src={require("./images/maestro.png")} alt="Maestro"/>
                    </li>
                </ul>
            </div>
            <div className="footer__bottom">
                <div className="footer__bottom-container container">
                    <p className="footer__copyright">2022-2023 All rights reserved</p>
                    <p className="footer__developed-by">Site is developed by ME</p>
                </div>
            </div>
        </footer>
    )
}