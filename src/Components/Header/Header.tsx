import React, {useContext} from "react";
import "./Header.scss"
import {GlobalContext} from "../../context";
import {Link} from "react-router-dom";

export default function Header() {
    const {logout, accountName} = useContext<any>(GlobalContext)

    return (
        <header className="header">
            <div className="header__container">
                <nav style={{display: "contents"}}><Link to="/" className="header__title">Lorem</Link></nav>
                <div className="header__account">
                    {
                        accountName
                    }
                    <button aria-label="Logout" onClick={() => logout()} className="header__logout-button">
                        <span className="header__logout-button-text">Logout</span>
                        <svg className="header__logout-button-icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8.90002 7.56C9.21002 3.96 11.06 2.49 15.11 2.49H15.24C19.71 2.49 21.5 4.28 21.5 8.75V15.27C21.5 19.74 19.71 21.53 15.24 21.53H15.11C11.09 21.53 9.24002 20.08 8.91002 16.54" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M2 12H14.88" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M12.65 8.65L16 12L12.65 15.35" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </button>
                </div>
            </div>
        </header>
    )
}