import {useEffect, useRef, useState} from "react";
import "./PreloaderImage.scss"

interface IPreloaderImage {
    className?: string;
    src: string;
    alt: string;
}

enum PreloaderImageLoadingStates {
    loading,
    loaded,
    error
}

export default function PreloaderImage({className, src, alt}: IPreloaderImage) {
    const imgRef = useRef<HTMLImageElement>(null)
    const [loadingState, setLoadingState] = useState<PreloaderImageLoadingStates>(PreloaderImageLoadingStates.loading)

    useEffect(() => {
        let img = imgRef.current

        if (!img) return
        else if (img.complete){
            setLoadingState(PreloaderImageLoadingStates.loaded)
            return
        }

        let progressEventListener = () => {
            setLoadingState(PreloaderImageLoadingStates.loading)
        }
        let loadEventListener = () => {
            setLoadingState(PreloaderImageLoadingStates.loaded)
        }
        let errorEventListener = () => {
            setLoadingState(PreloaderImageLoadingStates.error)
        }

        img.addEventListener("progress", progressEventListener)
        img.addEventListener("load", loadEventListener)
        img.addEventListener("error", errorEventListener)

        return () => {
            if (img) {
                img.removeEventListener("progress", progressEventListener)
                img.removeEventListener("load", loadEventListener)
                img.removeEventListener("error", errorEventListener)
            }
        }
    }, [])

    let classNameForLoadState: string

    switch (loadingState) {
        case(PreloaderImageLoadingStates.loading):
            classNameForLoadState = "preloader-image_loading"
            break;
        case(PreloaderImageLoadingStates.loaded):
            classNameForLoadState = "preloader-image_loaded"
            break;
        case(PreloaderImageLoadingStates.error):
            classNameForLoadState = "preloader-image_error"
            break;
    }

    return (
        <div className={["preloader-image", classNameForLoadState, className].join(" ")}>
            <img loading="lazy" ref={imgRef} className="preloader-image__image" src={src} alt={alt}/>
        </div>
    )
}