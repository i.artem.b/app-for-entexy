import "./Preloader.scss"

interface IPreloader{
    width?: number,
    height?: number,
    className?: string
}

export default function Preloader({width, height}:IPreloader) {
    return (
        <div className="preloader" style={{width, height}}>

        </div>
    )
}