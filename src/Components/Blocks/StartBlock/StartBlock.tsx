import React from "react";
import "./StartBlock.scss"

interface IStartBlockProps {
    scrollTarget: string
}

export default function StartBlock({scrollTarget}: IStartBlockProps) {

    const buttonAction = () => {
        let element = document.getElementById(scrollTarget)
        if (element) element.scrollIntoView({
            block: "nearest",
            behavior: "smooth"
        })
    }

    return (
        <section className="start-block">
            <div className="start-block__container container">
                <img aria-hidden="true" className="start-block__laptop-image" src={require("./images/macbook.webp")}
                     alt=""/>
                <img aria-hidden="true" className="start-block__text-image" src={require("./images/text.webp")} alt=""/>
                <div className="start-block__content">
                    <h1 className="start-block__header">Lorem ipsum<br/>dolor sit amet</h1>
                    <p className="start-block__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                    <button onClick={buttonAction} className="button start-block__button">to pictures</button>
                </div>
            </div>
        </section>
    )
}