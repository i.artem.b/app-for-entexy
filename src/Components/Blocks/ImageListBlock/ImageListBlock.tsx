import PreloaderImage from "../../UI/PreloaderImage/PreloaderImage";
import "./ImageListBlock.scss"
import {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import Preloader from "../../UI/Preloader/Preloader";


enum ImageListLoadingState {
    loading,
    loaded,
    error
}

export default function ImageListBlock({...props}) {
    const [loadingState, setLoadingState] = useState<ImageListLoadingState>(ImageListLoadingState.loading)
    const [imageList, setImageList] = useState<Array<any>>()

    useEffect(() => {
        fetch(`https://picsum.photos/v2/list?page=2&limit=12`)
            .then(data => data.json())
            .then(data => {
                setImageList(data)
                setLoadingState(ImageListLoadingState.loaded)
            })
            .catch(() => setLoadingState(ImageListLoadingState.error))
    }, [])

    const Layout = (
        <section {...props} className="image-list-block">
            <div className="image-list-block__container container">
                <h2 className="image-list-block__header">All images</h2>
                <ul className="image-list-block__list">
                    {
                        imageList?.map(elem => {
                            return (
                                <li key={elem.id} className="image-list-block__list-item">
                                    <Link to={`image/${elem.id}`} style={{display: "contents"}}>
                                        <PreloaderImage
                                            className="image-list-block__preloader-image"
                                            alt="alt"
                                            src={elem.download_url}
                                        />
                                    </Link>
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        </section>
    )

    const loadingLayout = <Preloader/>
    const errorLayout = <>Error 😵</>

    let show = loadingLayout

    switch (loadingState) {
        case ImageListLoadingState.loading:
            show = loadingLayout
            break
        case ImageListLoadingState.loaded:
            show = Layout
            break
        case ImageListLoadingState.error:
            show = errorLayout
    }

    return show
}