import Header from "./Components/Header/Header";
import Footer from "./Components/Footer/Footer";
import {createBrowserRouter, RouterProvider, Link, Outlet, Navigate} from "react-router-dom";
import {lazy, Suspense, useContext} from "react";
import {GlobalContext} from "./context";
import Preloader from "./Components/UI/Preloader/Preloader";

const LoginPage = lazy(() => import( "./Pages/LoginPage/LoginPage"));
const StartPage = lazy(() => import( "./Pages/StartPage/StartPage"));
const ImageInfoPage = lazy(() => import( "./Pages/ImageInfoPage/ImageInfoPage"));

export default function Layout() {

    let {checkLogin} = useContext<any>(GlobalContext)

    return <RouterProvider router={createBrowserRouter([
        {
            path: "/",
            element: (
                <>
                    <Header/>
                    <main>
                        <Outlet/>
                    </main>
                    <Footer/>
                </>
            ),
            errorElement: <>Error 404 😟 <Link to="/">Go to home</Link></>,
            children: [
                {
                    path: "",
                    element: <>
                        {
                            checkLogin()
                                ?
                                <Suspense fallback={<Preloader/>}>
                                    <StartPage/>
                                </Suspense>
                                :
                                <Navigate to="/login"/>
                        }
                    </>
                },
                {
                    path: "image/:id",
                    element: <>
                        {
                            checkLogin()
                                ?
                                <Suspense fallback={<Preloader/>}>
                                    <ImageInfoPage/>
                                </Suspense>
                                :
                                <Navigate to="/login"/>
                        }
                    </>
                }
            ]
        }, {
            path: "/login",
            element: <Suspense fallback={<Preloader/>}><LoginPage/></Suspense>
        }
    ])}/>
}